//------------------------------------------------
//--- 010 Editor v8.0 Binary Template
//
//      File: MK8_BIN.bt
//   Authors: Syroot
//   Version: 0.1.0
//   Purpose: Parse Mario Kart 8 (Deluxe) BIN files.
//  Category: Mario Kart 8
// File Mask: *.bin
//  ID Bytes: 
//   History: 
//  0.2.0   2017-09-13  Rewrite section data logic and add IndexedStringSectionData.
//  0.1.0   2017-09-12  Initial version.

// ==== Forward Declarations ===========================================================================================

struct BinFile;
struct Section;
struct DwordSectionData; struct DwordArray2D; struct DwordArray;
struct StringSectionData; struct StringSectionDataArray; struct String;
struct IndexedStringSectionData; struct IndexedStringSectionDataArray;

// ==== Includes =======================================================================================================

#include "Math.bt"

// ==== Structures =====================================================================================================

typedef uint Identifier <fgcolor=0x0000FF, read=IdentifierRead>;

typedef struct // BinFile
{
    Identifier id;
    uint fileSize; // Slightly off at times.
    ushort sectionCount;
    ushort headerSize;
    uint version; // Always 1000.
    int sectionOffsets[sectionCount] <format=hex>;
    // Read the sections.
    local int i <hidden=true>;
    local int sectionOffset <hidden=true>;
    local int sectionStart <hidden=true>;
    local int sectionEnd <hidden=true>;
    local int sectionSize <hidden=true>;
    for (i = 0; i < sectionCount; i++)
    {
        sectionOffset = sectionOffsets[i];
        sectionStart = headerSize + sectionOffset;
        sectionEnd = i == sectionCount - 1 ? FileSize() : headerSize + sectionOffsets[i + 1];
        sectionSize = sectionEnd - sectionStart;
        FSeek(sectionStart);
        Section section(sectionSize);
    }
} BinFile <bgcolor=0xCDFFFF>;

typedef struct(int sectionSize) // Section
{
    local int64 sectionStart <hidden=true> = FTell();
    Identifier id;
    ushort paramCount;
    ushort paramRepeat;
    if (isDeluxeFile) byte paramDeluxe[4];
    uint paramUnknown;
    local int sectionDataSize <hidden=true> = sectionSize - (FTell() - sectionStart);
    // Read typed data. Actually, ID should be used to switch on the type, but for RE uses paramUnknown is taken.
    switch (paramUnknown)
    {
        case 0: DwordSectionData data(this, sectionDataSize); break;
        case 76: IndexedStringSectionData data(this); break;
        case 160: StringSectionData data(this); break;
        default: Warning("Unknown section type encountered."); break;
    }
    FAlign(4);
} Section <bgcolor=0xBDEBEB, read=SectionRead>;

// ---- DwordSectionData -----------------------------------------------------------------------------------------------

typedef struct(Section &section, int sectionDataSize) // DwordSectionData
{
    local int elementCount <hidden=true> = sectionDataSize / section.paramRepeat / section.paramCount / sizeof(int);
    DwordArray2D array(section.paramCount, elementCount)[section.paramRepeat] <optimize=true>;
} DwordSectionData <bgcolor=0xCDE6FF>;

typedef struct(int size1, int size2) // DwordArray2D
{
    DwordArray array(size2)[size1] <optimize=true>;
} DwordArray2D;

typedef struct(int size) // DwordArray
{
    Dword array[size];
} DwordArray;

// ---- StringSectionData ----------------------------------------------------------------------------------------------

typedef struct(Section &section) // StringSectionData
{
    StringSectionDataArray array(section.paramCount)[section.paramRepeat] <optimize=false>;
} StringSectionData <bgcolor=0xCDCDFF>;

typedef struct(int paramCount) // StringSectionDataArray
{
    // Read offsets.
    int offsets[paramCount];
    local int64 offsetBase <hidden=true> = FTell();
    // Read strings.
    local int i <hidden=true>;
    for (i = 0; i < paramCount; i++)
    {
        FSeek(offsetBase + offsets[i]);
        String str;
    }
} StringSectionDataArray;

typedef struct // String
{
    string str;
} String <read=StringRead>;

// ---- IndexedStringSectionData ---------------------------------------------------------------------------------------

typedef struct(Section &section) // IndexedStringSectionData
{
    IndexedStringSectionDataArray array(section.paramCount)[section.paramRepeat] <optimize=false>;
} IndexedStringSectionData <bgcolor=0xBDBDEB>;

typedef struct(int paramCount) // IndexedStringSectionDataArray
{
    // Read offsets.
    local int i <hidden=true>;
    for (i = 0; i < paramCount; i++)
    {
        byte index;
        FAlign(4);
        int offset;
    }
    local int64 offsetBase <hidden=true> = FTell();
    // Read strings.
    for (i = 0; i < paramCount; i++)
    {
        FSeek(offsetBase + offset[i]);
        String str;
    }
} IndexedStringSectionDataArray;

// ==== File Contents ==================================================================================================

// Detect if this is an MK8U or MK8D file.
LittleEndian();
local ubyte isDeluxeFile = ReadInt(12) == 1000;
if (!isDeluxeFile) BigEndian();

BinFile file <open=true>;

// ==== Template Methods ===============================================================================================

string IdentifierRead(Identifier &value)
{
    // Get the bytes of the value.
    uchar bytes[4];
    BigEndian();
    ConvertDataToBytes(value, bytes);
    if (isDeluxeFile) LittleEndian();
    return bytes;
}

string SectionRead(Section &value)
{
    return IdentifierRead(value.id);
}

string StringRead(String &value)
{
    return value.str;
}