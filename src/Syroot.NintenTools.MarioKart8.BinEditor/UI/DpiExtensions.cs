﻿using System.Drawing;
using System.Windows.Forms;

namespace Syroot.NintenTools.MarioKart8.BinEditor.UI
{
    /// <summary>
    /// Represents a collection of extension methods to simplify DPI calculations.
    /// </summary>
    internal static class DpiExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static Padding Scale(this Padding padding, SizeF factor)
        {
            return new Padding(
                (int)(padding.Left * factor.Width),
                (int)(padding.Top * factor.Height),
                (int)(padding.Right * factor.Width),
                (int)(padding.Bottom * factor.Height));
        }

        internal static Size Scale(this Size size, SizeF factor)
        {
            return new Size((int)(size.Width * factor.Width), (int)(size.Height * factor.Height));
        }
    }
}
