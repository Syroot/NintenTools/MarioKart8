﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Syroot.NintenTools.MarioKart8.BinEditor.UI
{
    /// <summary>
    /// Represents a hierarchy in horizontal bars out of which only the currently selected path is visible.
    /// </summary>
    [ToolboxBitmap(typeof(TreeView))]
    public class RowTree : Control
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private RowTreeNode _selectedNode;
        private RowTreeNode _hoveredNode;
        private List<RowTreeNode> _selectedPathNodes;

        private SizeF _scaleFactor;
        private Color _backColorHovered;
        private Color _backColorSelected;
        private Color _foreColorDisabled;
        private float _colorGain;
        private int _rowHeight;

        private SolidBrush _brBackColor;
        private SolidBrush _brBackColorHovered;
        private SolidBrush _brBackColorSelected;
        private SolidBrush _brGain;
        private StringFormat _sfText;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="RowTree"/> class.
        /// </summary>
        public RowTree()
        {
            SetStyle(
                ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw,
                true);

            // Set default values.
            RootNode = new RowTreeNode();
            SelectedNode = RootNode;
            BackColor = Color.FromArgb(32, 32, 32);
            BackColorHovered = Color.FromArgb(12, 255, 255, 255);
            BackColorSelected = Color.FromArgb(0, 66, 200);
            ForeColor = Color.White;
            ForeColorDisabled = Color.FromArgb(64, 255, 255, 255);
            ColorGain = 0.085f;
            RowHeight = 30;

            _sfText = new StringFormat
            {
                Alignment = StringAlignment.Center,
                FormatFlags = StringFormatFlags.NoWrap,
                LineAlignment = StringAlignment.Center
            };
            CreateBackColorBrush();
            CreateBackColorHoveredBrush();
        }

        // ---- EVENTS -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Occurs when the <see cref="SelectedNode"/> is about to change.
        /// </summary>
        public event EventHandler<RowTreeNodeChangingEventArgs> SelectedNodeChanging;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets a value indicating whether the control is automatically sized to fit its contents.
        /// </summary>
        [Browsable(true)]
        [Category("Layout")]
        [DefaultValue(false)]
        [Description("Indicates whether the control is automatically sized to fit its contents.")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [RefreshProperties(RefreshProperties.All)]
        public override bool AutoSize
        {
            get { return base.AutoSize; }
            set { base.AutoSize = value; }
        }

        /// <summary>
        /// Gets or sets the background color of nodes.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(typeof(Color), "32, 32, 32")]
        [Description("The background color of nodes.")]
        public override Color BackColor
        {
            get { return base.BackColor; }
            set
            {
                base.BackColor = value;
                CreateBackColorBrush();
            }
        }

        /// <summary>
        /// Gets or sets the additive background color of the currently hovered node.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(typeof(Color), "12, 255, 255, 255")]
        [Description("The additive background color of the currently hovered node.")]
        public Color BackColorHovered
        {
            get { return _backColorHovered; }
            set
            {
                _backColorHovered = value;
                CreateBackColorHoveredBrush();
                Refresh();
            }
        }

        /// <summary>
        /// Gets or sets the background color of nodes forming the selected path.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(typeof(Color), "0, 66, 200")]
        [Description("The background color of nodes forming the selected path.")]
        public Color BackColorSelected
        {
            get { return _backColorSelected; }
            set
            {
                _backColorSelected = value;
                CreateBackColorSelectedBrush();
                Refresh();
            }
        }

        /// <summary>
        /// Gets or sets the text color of nodes.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(typeof(Color), "White")]
        [Description("The text color of nodes.")]
        public override Color ForeColor
        {
            get { return base.ForeColor; }
            set { base.ForeColor = value; }
        }

        /// <summary>
        /// Gets or sets the text color of disabled nodes.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(typeof(Color), "64, 255, 255, 255")]
        [Description("The text color of disabled nodes.")]
        public Color ForeColorDisabled
        {
            get { return _foreColorDisabled; }
            set
            {
                _foreColorDisabled = value;
                Refresh();
            }
        }

        /// <summary>
        /// Gets or sets the factor with which colors are brightened (range 0 to 1) or darkened (range -1 to 0) in
        /// deeper hierarchy levels.
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(0.085f)]
        [Description("The factor with which colors are brightened or darkened in deeper hierarchy levels.")]
        public float ColorGain
        {
            get { return _colorGain; }
            set
            {
                if (value < -1 || value > 1)
                    throw new ArgumentOutOfRangeException("Color gain must lie between -1 and 1.", nameof(value));

                _colorGain = value;
                CreateGainBrush();
                Refresh();
            }
        }

        /// <summary>
        /// Gets or sets the height of a hierarchy level.
        /// </summary>
        [Category("Layout")]
        [DefaultValue(30)]
        [Description("The height of a hierarchy level.")]
        public int RowHeight
        {
            get { return _rowHeight; }
            set
            {
                _rowHeight = value;
                Refresh();
            }
        }

        /// <summary>
        /// Gets the root <see cref="RowTreeNode"/> which children are shown as the first level of the hiearchy.
        /// </summary>
        [Browsable(false)]
        public RowTreeNode RootNode { get; }

        /// <summary>
        /// Gets the currently selected node.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public RowTreeNode SelectedNode
        {
            get { return _selectedNode; }
            set
            {
                // Raise an event which can adjust the new node to select.
                RowTreeNodeChangingEventArgs e = new RowTreeNodeChangingEventArgs(_selectedNode, value);
                SelectedNodeChanging?.Invoke(this, e);
                value = e.NewNode;

                _selectedNode = value ?? throw new ArgumentNullException(nameof(value));

                // Update the nodes forming the currently selected path.
                _selectedPathNodes = new List<RowTreeNode>();
                RowTreeNode currentNode = SelectedNode;
                while (currentNode != null)
                {
                    _selectedPathNodes.Insert(0, currentNode);
                    currentNode = currentNode.Parent;
                }

                AdjustSize();
                Refresh();
            }
        }

        /// <summary>
        /// Gets or sets the text, not used.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string Text
        {
            get { return null; }
            set { }
        }

        private RowTreeNode HoveredNode
        {
            get { return _hoveredNode; }
            set
            {
                _hoveredNode = value;
                Refresh();
            }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Resizes the control so it is only as tall as all visible rows. Calling this method is required if the node
        /// hierarchy changes, as it is not observed.
        /// </summary>
        public void AdjustSize()
        {
            Size = GetPreferredSize(new Size());
        }

        /// <summary>
        /// Retrieves the size of a rectangular area into which a control can be fitted.
        /// </summary>
        /// <param name="proposedSize">The custom-sized area for a control.</param>
        /// <returns>A <see cref="Size"/> representing the width and height of a rectangle.</returns>
        public override Size GetPreferredSize(Size proposedSize)
        {
            // Compute the number of visible hierarchy levels.
            int visibleLevels = _selectedPathNodes.Count;
            if (_selectedPathNodes[visibleLevels - 1].IsLeaf)
                visibleLevels--;
            visibleLevels = Math.Max(visibleLevels, 1); // Show at least 1 level for now.

            // Return an according size.
            int scaledRowHeight = (int)(RowHeight * _scaleFactor.Height);
            return new Size(Width, visibleLevels * scaledRowHeight);
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Raised when the mouse pointer enters the control bounds.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/>.</param>
        protected override void OnMouseEnter(EventArgs e)
        {
            HoveredNode = GetNodeAt(PointToClient(Cursor.Position));
            base.OnMouseEnter(e);
        }

        /// <summary>
        /// Raised when the mouse pointer moves inside the control bounds.
        /// </summary>
        /// <param name="e">The <see cref="MouseEventArgs"/>.</param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            HoveredNode = GetNodeAt(PointToClient(Cursor.Position));
            base.OnMouseMove(e);
        }

        /// <summary>
        /// Raised when the mouse pointer leaves the control bounds.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/>.</param>
        protected override void OnMouseLeave(EventArgs e)
        {
            HoveredNode = null;
            base.OnMouseLeave(e);
        }

        /// <summary>
        /// Raised when the mouse pointer clicks inside the control bounds.
        /// </summary>
        /// <param name="e">The <see cref="MouseEventArgs"/>.</param>
        protected override void OnMouseClick(MouseEventArgs e)
        {
            // Must be inside the control.
            if (e.Button == MouseButtons.Left && HoveredNode != null && HoveredNode != SelectedNode)
            {
                SelectedNode = HoveredNode;
            }
            base.OnMouseClick(e);
        }

        /// <summary>
        /// Raised when the control has to paint its contents.
        /// </summary>
        /// <param name="e">The <see cref="PaintEventArgs"/>.</param>
        protected override void OnPaint(PaintEventArgs e)
        {
            // Go through all nodes forming the selected path.
            int scaledRowHeight = (int)(RowHeight * _scaleFactor.Height);
            for (int i = 0; i < _selectedPathNodes.Count; i++)
            {
                RowTreeNode node = _selectedPathNodes[i];
                if (node.IsLeaf) break;

                // Draw the children of the current node.
                int currentY = i * scaledRowHeight;
                float nodeWidth = Width / (float)node.Children.Count;
                float currentX = 0;
                for (int j = 0; j < node.Children.Count; j++)
                {
                    RowTreeNode childNode = node.Children[j];
                    Rectangle nodeRect = new Rectangle(
                        (int)Math.Ceiling(currentX), currentY, (int)Math.Ceiling(nodeWidth), scaledRowHeight);

                    // Draw the background.
                    SolidBrush br = _selectedPathNodes.Contains(childNode) ? _brBackColorSelected : _brBackColor;
                    e.Graphics.FillRectangle(br, nodeRect);
                    // Apply gain on the background.
                    for (int k = 0; k < i; k++)
                    {
                        e.Graphics.FillRectangle(_brGain, nodeRect);
                    }
                    // Apply the hover color.
                    if (childNode.Enabled && childNode == _hoveredNode)
                        e.Graphics.FillRectangle(_brBackColorHovered, nodeRect);

                    // Draw the text.
                    Color foreColor = childNode.Enabled ? ForeColor : ForeColorDisabled;
                    TextRenderer.DrawText(e.Graphics, childNode.Text, Font, nodeRect, foreColor,
                        TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter);

                    currentX += nodeWidth;
                }
            }
        }

        /// <summary>
        /// Scales a control's location, size, padding and margin.
        /// </summary>
        /// <param name="factor">The factor by which the height and width of the control will be scaled.</param>
        /// <param name="specified">A <see cref="BoundsSpecified"/> value that specifies the bounds of the control to
        /// use when defining its size and position.</param>
        protected override void ScaleControl(SizeF factor, BoundsSpecified specified)
        {
            base.ScaleControl(factor, specified);
            _scaleFactor = factor;
        }

        /// <summary>
        /// Performs the work of setting the specified bounds of this control.
        /// </summary>
        /// <param name="x">The new <see cref="Control.Left"/> property value of the control.</param>
        /// <param name="y">The new <see cref="Control.Top"/> property value of the control.</param>
        /// <param name="width">The new <see cref="Control.Width"/> property value of the control.</param>
        /// <param name="height">The new <see cref="Control.Width"/> property value of the control.</param>
        /// <param name="specified">A bitwise combination of the <see cref="BoundsSpecified"/> values.</param>
        protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
        {
            // When AutoSize is enabled, scale the control to be only as tall as all visible rows.
            if (AutoSize && specified.HasFlag(BoundsSpecified.Size))
            {
                Size size = GetPreferredSize(new Size());
                height = size.Height;
            }
            base.SetBoundsCore(x, y, width, height, specified);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void CreateBackColorBrush()
        {
            _brBackColor = new SolidBrush(BackColor);
        }

        private void CreateBackColorHoveredBrush()
        {
            _brBackColorHovered = new SolidBrush(BackColorHovered);
        }

        private void CreateBackColorSelectedBrush()
        {
            _brBackColorSelected = new SolidBrush(BackColorSelected);
        }

        private void CreateGainBrush()
        {
            if (_colorGain < 0)
                _brGain = new SolidBrush(Color.FromArgb((int)(-_colorGain * 255), Color.Black));
            else
                _brGain = new SolidBrush(Color.FromArgb((int)(_colorGain * 255), Color.White));
        }

        private RowTreeNode GetNodeAt(Point position)
        {
            // Must be inside the control.
            if (!ClientRectangle.IntersectsWith(new Rectangle(position, new Size(1, 1))))
                return null;

            int scaledRowHeight = (int)(RowHeight * _scaleFactor.Height);
            for (int i = 0; i < _selectedPathNodes.Count; i++)
            {
                RowTreeNode node = _selectedPathNodes[i];
                if (node.IsLeaf) break;

                // Check if the mouse is in the current hierarchy level.
                if (position.Y >= i * scaledRowHeight && position.Y <= (i + 1) * scaledRowHeight)
                {
                    // Get the child of the hierarchy level.
                    float nodeWidth = Width / (float)node.Children.Count;
                    float currentX = 0;
                    for (int j = 0; j < node.Children.Count; j++)
                    {
                        int x = (int)Math.Ceiling(currentX);
                        if (position.X >= x && position.X <= x + Math.Ceiling(nodeWidth))
                        {
                            return node.Children[j];
                        }
                        currentX += nodeWidth;
                    }
                }
            }
            return null;
        }
    }

    /// <summary>
    /// Represents the <see cref="EventArgs"/> passed to <see cref="RowTreeNode"/> changing events.
    /// </summary>
    public class RowTreeNodeChangingEventArgs : EventArgs
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="RowTreeNodeChangingEventArgs"/> with the given details.
        /// </summary>
        /// <param name="currentNode">The previously selected <see cref="RowTreeNode"/>.</param>
        /// <param name="newNode">The <see cref="RowTreeNode"/> to select.</param>
        internal RowTreeNodeChangingEventArgs(RowTreeNode currentNode, RowTreeNode newNode)
        {
            OldNode = currentNode;
            NewNode = newNode;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the previously selected <see cref="RowTreeNode"/>.
        /// </summary>
        public RowTreeNode OldNode { get; }

        /// <summary>
        /// Gets or sets the <see cref="RowTreeNode"/> to select.
        /// </summary>
        public RowTreeNode NewNode { get; set; }
    }
}
