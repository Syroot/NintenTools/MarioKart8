﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Generic;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.Editors
{
    /// <summary>
    /// Represents a generic editor trying to display tabular data from unknown BIN files.
    /// </summary>
    internal class GenericEditor : EditorBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the accentual color used for category rows in the user interface.
        /// </summary>
        internal override Color AccentColor
        {
            get { return Color.FromArgb(200, 0, 0); }
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Returns whether the editor can edit the given BIN file.
        /// </summary>
        /// <param name="binFile">The <see cref="BinFile"/> to check.</param>
        internal override bool CheckBinFile(BinFile binFile)
        {
            // The BIN file must have at least one section which can be supported by the experimental view.
            if (GetDwordSectionData(binFile).Any())
            {
                MessageBox.Show("This BIN file is not supported, but can be opened in an experimental view."
                + " The data might be wrongly presented or incomplete.", AssemblyInfo.Title, MessageBoxButtons.OK,
                MessageBoxIcon.Information);
                return true;
            }
            return false;
        }
        
        /// <summary>
        /// Gets the <see cref="RowTreeNode"/> of the first hierarchy level to show for navigation.
        /// </summary>
        /// <returns>The <see cref="RowTreeNode"/> to display.</returns>
        internal override RowTreeNode GetCategoryTree()
        {
            RowTreeNode root = new RowTreeNode();
            foreach (Section section in GetDwordSectionData(Program.App.BinFile))
            {
                // Create a node for the section.
                DwordSectionData sectionData = (DwordSectionData)section.Data;
                RowTreeNode sectionNode = new RowTreeNode(section.Name);
                if (sectionData.Data.Length == 1)
                {
                    // The section has only one table, display it immediately.
                    sectionNode.Tag = new GenericDataAdapter(sectionData.Data[0]);
                }
                else
                {
                    // The section has multiple tables, create a subnode for each.
                    for (int i = 0; i < sectionData.Data.Length; i++)
                    {
                        RowTreeNode childNode = new RowTreeNode((i + 1).ToString());
                        childNode.Tag = new GenericDataAdapter(sectionData.Data[i]);
                        sectionNode.Children.Add(childNode);
                    }
                }
                root.Children.Add(sectionNode);
            }
            return root;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private IEnumerable<Section> GetDwordSectionData(BinFile binFile)
        {
            foreach (Section section in Program.App.BinFile.Sections)
            {
                if (section.Data is DwordSectionData sectionData)
                    yield return section;
            }
        }
    }
}
