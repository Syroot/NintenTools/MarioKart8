using System;
using System.Collections.Generic;
using System.Linq;
using Syroot.NintenTools.MarioKart8.BinData.Item;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Item
{
    internal class BattleDataAdapter : ItemDataAdapterBase
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly List<TableHeader> _rows = new List<TableHeader>()
        {
            new TableHeader($"> 2 Minutes{Environment.NewLine}> 2 Balloons"),
            new TableHeader($"> 2 Minutes{Environment.NewLine}2 Balloons"),
            new TableHeader($"> 2 Minutes{Environment.NewLine}1 Balloon"),
            new TableHeader($"> 1 Minute{Environment.NewLine}> 2 Balloons"),
            new TableHeader($"> 1 Minute{Environment.NewLine}2 Balloons"),
            new TableHeader($"> 1 Minute{Environment.NewLine}1 Balloon"),
            new TableHeader($"> 0 Minutes{Environment.NewLine}> 2 Balloons"),
            new TableHeader($"> 0 Minutes{Environment.NewLine}2 Balloons"),
            new TableHeader($"> 0 Minutes{Environment.NewLine}1 Balloon"),
            new TableHeader($"Any Minute{Environment.NewLine}0 Balloons"),
        };

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _isAISet;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal BattleDataAdapter(SectionIdentifier sectionIdentifier, BattleItemSet set, bool isAISet)
            : base(sectionIdentifier, (int)set)
        {
            _isAISet = isAISet;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override IEnumerable<TableHeader> Rows
        {
            get
            {
                return _rows.Take(Array.Length);
            }
        }

        protected internal override string RowHeaderTitle
        {
            get { return "State"; }
        }
    }
}
