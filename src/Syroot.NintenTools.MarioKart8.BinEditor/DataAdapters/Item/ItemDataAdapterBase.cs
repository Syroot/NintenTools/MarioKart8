using System.Collections.Generic;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinData.Item;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Item
{
    internal abstract class ItemDataAdapterBase : DataAdapter
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private SectionIdentifier _sectionIdentifier;
        private int _setIndex;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal ItemDataAdapterBase(SectionIdentifier section, int setIndex)
        {
            _sectionIdentifier = section;
            _setIndex = setIndex;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override IEnumerable<TableHeader> Columns
        {
            get
            {
                yield return new TableHeader("Banana", Program.R.GetBitmap("Items.Banana.png"));
                yield return new TableHeader("Green Shell", Program.R.GetBitmap("Items.ShellGreen.png"));
                yield return new TableHeader("Red Shell", Program.R.GetBitmap("Items.ShellRed.png"));
                yield return new TableHeader("Mushroom", Program.R.GetBitmap("Items.Mushroom.png"));
                yield return new TableHeader("Bob-omb", Program.R.GetBitmap("Items.Bobomb.png"));
                yield return new TableHeader("Blooper", Program.R.GetBitmap("Items.Blooper.png"));
                yield return new TableHeader("Spiny Shell", Program.R.GetBitmap("Items.ShellBlue.png"));
                yield return new TableHeader("3 Mushrooms", Program.R.GetBitmap("Items.Mushroom3.png"));
                yield return new TableHeader("Star", Program.R.GetBitmap("Items.Star.png"));
                yield return new TableHeader("Bullet Bill", Program.R.GetBitmap("Items.Bullet.png"));
                yield return new TableHeader("Lightning Bolt", Program.R.GetBitmap("Items.Lightning.png"));
                yield return new TableHeader("Gold Mushroom", Program.R.GetBitmap("Items.MushroomGold.png"));
                yield return new TableHeader("Fire Flower", Program.R.GetBitmap("Items.Flower.png"));
                yield return new TableHeader("Piranha Plant", Program.R.GetBitmap("Items.Piranha.png"));
                yield return new TableHeader("Super Horn", Program.R.GetBitmap("Items.Horn.png"));
                yield return new TableHeader("Boomerang", Program.R.GetBitmap("Items.Boomerang.png"));
                yield return new TableHeader("Coin", Program.R.GetBitmap("Items.Coin.png"));
                yield return new TableHeader("3 Bananas", Program.R.GetBitmap("Items.Banana3.png"));
                yield return new TableHeader("3 Green Shells", Program.R.GetBitmap("Items.ShellGreen3.png"));
                yield return new TableHeader("3 Red Shells", Program.R.GetBitmap("Items.ShellRed3.png"));
                if (Program.App.BinFile.Format == BinFileFormat.MarioKart8)
                {
                    yield return new TableHeader("Crazy Eight", Program.R.GetBitmap("Items.Eight.png"));
                }
                else
                {
                    yield return new TableHeader("Crazy Eight", Program.R.GetBitmap("Items.EightDeluxe.png"));
                    yield return new TableHeader("Feather", Program.R.GetBitmap("Items.Feather.png"));
                    yield return new TableHeader("Boo", Program.R.GetBitmap("Items.Boo.png"));
                }
            }
        }

        protected internal override string RowHeaderTitle
        {
            get { return "Distance"; }
        }
        
        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)_sectionIdentifier);
                return ((DwordSectionData)section.Data).Data[_setIndex];
            }
        }
    }
}
