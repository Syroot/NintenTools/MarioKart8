﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Generic
{
    /// <summary>
    /// Represents a generic view on a 2-dimensional <see cref="Dword"/> table.
    /// </summary>
    internal class GenericDataAdapter : DataAdapter
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Dword[][] _array;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal GenericDataAdapter(Dword[][] array, bool transposed = false)
        {
            _array = array;
            Transposed = transposed;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the 2-dimensional <see cref="Dword"/> array from which the data is retrieved and stored.
        /// </summary>
        protected internal override Dword[][] Array
        {
            get { return _array; }
        }

        /// <summary>
        /// Gets or sets a text displayed at in top left header cell.
        /// </summary>
        protected internal override string RowHeaderTitle
        {
            get { return "Row"; }
        }

        /// <summary>
        /// Gets or sets the enumeration of <see cref="TableHeader"/> instances used to display column headers.
        /// </summary>
        protected internal override IEnumerable<TableHeader> Columns
        {
            get
            {
                int length = Transposed ? _array.Length : _array[0].Length;
                for (int i = 0; i < length; i++)
                {
                    yield return new TableHeader(GetColumnName(i));
                }
            }
        }

        /// <summary>
        /// Gets or sets the enumeration of <see cref="TableHeader"/> instances used to display row headers.
        /// </summary>
        protected internal override IEnumerable<TableHeader> Rows
        {
            get
            {
                int length = Transposed ? _array[0].Length : _array.Length;
                for (int i = 0; i < length; i++)
                {
                    yield return new TableHeader((i + 1).ToString());
                }
            }
        }

        /// <summary>
        /// Returns a value determining whether the column at the corresponding <paramref name="index"/> stores float
        /// values.
        /// </summary>
        /// <param name="index">The index of the column.</param>
        /// <returns><c>true</c> if the column stores floating point values, <c>false</c> for integral values.</returns>
        protected internal override bool IsFloatColumn(int index)
        {
            // Check if the majority of Dword values in this column is a possible float.
            int floatValueCount = 0;
            int length = Transposed ? _array[0].Length : _array.Length;
            for (int i = 0; i < length; i++)
            {
                if (IsFloatValue(GetValue(index, i)))
                    floatValueCount++;
            }
            return floatValueCount > _array.Length / 2;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static string GetColumnName(int columnIndex)
        {
            int divisor = columnIndex + 1;
            string columnName = String.Empty;
            while (divisor > 0)
            {
                int modulo = (divisor - 1) % 26;
                columnName = (char)(65 + modulo) + columnName;
                divisor = (divisor - modulo) / 26;
            }
            return columnName;
        }

        private bool IsFloatValue(int value)
        {
            // Use a very dumb "algorithm" to check if the resulting integer would be "too long".
            return value.ToString().Length > 6;
        }
    }
}
