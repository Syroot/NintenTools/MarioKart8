using System.Collections.Generic;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Performance
{
    internal abstract class RanksDataAdapterBase : DataAdapter
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override IEnumerable<TableHeader> Rows
        {
            get
            {
                // Valid point ranks and one for the fallback value.
                for (int i = 0; i < Array.GetLength(0); i++)
                {
                    if (i == Array.GetLength(0) - 1)
                        yield return new TableHeader("Fallback");
                    else
                        yield return new TableHeader($"{i + 1} Point{(i == 0 ? null : "s")}");
                }
            }
        }

        protected internal override string RowHeaderTitle
        {
            get { return "Total Points"; }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected internal override bool IsFloatColumn(int index)
        {
            return true;
        }
    }
}
