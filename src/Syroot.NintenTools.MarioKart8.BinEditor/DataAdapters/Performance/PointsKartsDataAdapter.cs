using System.Collections.Generic;
using System.Linq;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinData.Performance;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Performance
{
    internal class PointsKartsDataAdapter : PointsDataAdapterBase
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly List<TableHeader> _rows = new List<TableHeader>()
        {
            new TableHeader("Standard Kart", Program.R.GetBitmap("Karts.StandardKart.png")),
            new TableHeader("Pipe Frame", Program.R.GetBitmap("Karts.PipeFrame.png")),
            new TableHeader("Mach 8", Program.R.GetBitmap("Karts.Mach8.png")),
            new TableHeader("Steel Driver", Program.R.GetBitmap("Karts.SteelDriver.png")),
            new TableHeader("Cat Cruiser", Program.R.GetBitmap("Karts.CatCruiser.png")),
            new TableHeader("Circuit Special", Program.R.GetBitmap("Karts.CircuitSpecial.png")),
            new TableHeader("Tri-Speeder", Program.R.GetBitmap("Karts.TriSpeeder.png")),
            new TableHeader("Badwagon", Program.R.GetBitmap("Karts.Badwagon.png")),
            new TableHeader("Prancer", Program.R.GetBitmap("Karts.Prancer.png")),
            new TableHeader("Biddybuggy", Program.R.GetBitmap("Karts.Biddybuggy.png")),
            new TableHeader("Landship", Program.R.GetBitmap("Karts.Landship.png")),
            new TableHeader("Sneeker", Program.R.GetBitmap("Karts.Sneeker.png")),
            new TableHeader("Sports Coupe", Program.R.GetBitmap("Karts.SportsCoupe.png")),
            new TableHeader("Gold Standard", Program.R.GetBitmap("Karts.GoldStandard.png")),
            new TableHeader("Standard Bike", Program.R.GetBitmap("Karts.StandardBike.png")),
            new TableHeader("Comet", Program.R.GetBitmap("Karts.Comet.png")),
            new TableHeader("Sport Bike", Program.R.GetBitmap("Karts.SportBike.png")),
            new TableHeader("The Duke", Program.R.GetBitmap("Karts.TheDuke.png")),
            new TableHeader("Flame Rider", Program.R.GetBitmap("Karts.FlameRider.png")),
            new TableHeader("Varmint", Program.R.GetBitmap("Karts.Varmint.png")),
            new TableHeader("Mr. Scooty", Program.R.GetBitmap("Karts.MrScooty.png")),
            new TableHeader("Jet Bike", Program.R.GetBitmap("Karts.JetBike.png")),
            new TableHeader("Yoshi Bike", Program.R.GetBitmap("Karts.YoshiBike.png")),
            new TableHeader("Standard ATV", Program.R.GetBitmap("Karts.StandardAtv.png")),
            new TableHeader("Wild Wiggler", Program.R.GetBitmap("Karts.WildWiggler.png")),
            new TableHeader("Teddy Buggy", Program.R.GetBitmap("Karts.TeddyBuggy.png")),
            new TableHeader("GLA", Program.R.GetBitmap("Karts.Gla.png")),
            new TableHeader("W 25 Silver Arrow", Program.R.GetBitmap("Karts.W25SilverArrow.png")),
            new TableHeader("300 SL Roadster", Program.R.GetBitmap("Karts.300SlRoadster.png")),
            new TableHeader("Blue Falcon", Program.R.GetBitmap("Karts.BlueFalcon.png")),
            new TableHeader("Tanooki Kart", Program.R.GetBitmap("Karts.TanookiKart.png")),
            new TableHeader("B Dasher", Program.R.GetBitmap("Karts.BDasher.png")),
            new TableHeader("Master Cycle", Program.R.GetBitmap("Karts.MasterCycle.png"))
        };

        private static readonly List<TableHeader> _rowsAddMK8U = new List<TableHeader>()
        {
            new TableHeader("Unused 1", Program.R.GetBitmap("Karts.Unknown.png")),
            new TableHeader("Unused 2", Program.R.GetBitmap("Karts.Unknown.png")),
            new TableHeader("Streetle", Program.R.GetBitmap("Karts.Streetle.png")),
            new TableHeader("P-Wing", Program.R.GetBitmap("Karts.PWing.png")),
            new TableHeader("City Tripper", Program.R.GetBitmap("Karts.CityTripper.png")),
            new TableHeader("Bone Rattler", Program.R.GetBitmap("Karts.BoneRattler.png"))
        };

        private static readonly List<TableHeader> _rowsAddMK8D = new List<TableHeader>()
        {
            new TableHeader("Streetle", Program.R.GetBitmap("Karts.Streetle.png")),
            new TableHeader("P-Wing", Program.R.GetBitmap("Karts.PWing.png")),
            new TableHeader("City Tripper", Program.R.GetBitmap("Karts.CityTripper.png")),
            new TableHeader("Bone Rattler", Program.R.GetBitmap("Karts.BoneRattler.png")),
            new TableHeader("Koopa Clown", Program.R.GetBitmap("Karts.KoopaClown.png")),
            new TableHeader("Splat Buggy", Program.R.GetBitmap("Karts.SplatBuggy.png")),
            new TableHeader("Inkstriker", Program.R.GetBitmap("Karts.Inkstriker.png"))
        };

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.KartPoints);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }

        protected internal override IEnumerable<TableHeader> Rows
        {
            get
            {
                List<TableHeader> rows = new List<TableHeader>(_rows);
                if (Program.App.BinFile.Format == BinFileFormat.MarioKart8)
                    rows.AddRange(_rowsAddMK8U);
                else
                    rows.AddRange(_rowsAddMK8D);
                return rows.Take(Array.GetLength(0));
            }
        }

        protected internal override string RowHeaderTitle
        {
            get { return "Kart"; }
        }
    }
}
