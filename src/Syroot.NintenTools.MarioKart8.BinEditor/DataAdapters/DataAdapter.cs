using System.Collections.Generic;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters
{
    /// <summary>
    /// Represents a data storage storing <see cref="Dword"/> values retrieved from a 2-dimensional <see cref="Dword"/>
    /// array which can be displayed in a table.
    /// </summary>
    internal abstract class DataAdapter
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the 2-dimensional <see cref="Dword"/> array from which the data is retrieved and stored.
        /// </summary>
        protected internal abstract Dword[][] Array { get; }
        
        /// <summary>
        /// Gets or sets a text displayed at in top left header cell.
        /// </summary>
        protected internal virtual string RowHeaderTitle { get; }

        /// <summary>
        /// Gets or sets the enumeration of <see cref="TableHeader"/> instances used to display column headers.
        /// </summary>
        protected internal abstract IEnumerable<TableHeader> Columns { get; }

        /// <summary>
        /// Gets or sets the enumeration of <see cref="TableHeader"/> instances used to display row headers.
        /// </summary>
        protected internal abstract IEnumerable<TableHeader> Rows { get; }

        /// <summary>
        /// Gets or sets a value whether columns and rows are transposed.
        /// </summary>
        protected internal bool Transposed { get; protected set; }

        // ---- METHODS (PROTECTED INTERNAL) ---------------------------------------------------------------------------

        /// <summary>
        /// Returns a value determining whether the column at the corresponding <paramref name="index"/> stores float
        /// values.
        /// </summary>
        /// <param name="index">The index of the column.</param>
        /// <returns><c>true</c> if the column stores floating point values, <c>false</c> for integral values.</returns>
        protected internal virtual bool IsFloatColumn(int index) { return false; }

        // ---- METHODS (PROTECTED INTERNAL) ---------------------------------------------------------------------------
        
        /// <summary>
        /// Gets a value for the cell at the given coordinates.
        /// </summary>
        /// <param name="x">The X coordinate of the cell.</param>
        /// <param name="y">The Y coordinate of the cell.</param>
        /// <returns>The value for the cell.</returns>
        protected internal virtual Dword GetValue(int x, int y)
        {
            if (Transposed)
                return Array[x][y];
            else
                return Array[y][x];
        }

        /// <summary>
        /// Sets the <paramref name="value"/> for the cell at the given coordinates.
        /// </summary>
        /// <param name="x">The X coordinate of the cell.</param>
        /// <param name="y">The Y coordinate of the cell.</param>
        /// <param name="value">The value to set.</param>
        protected internal virtual void SetValue(int x, int y, Dword value)
        {
            if (Transposed)
                Array[x][y] = value;
            else
                Array[y][x] = value;
        }
    }
}
