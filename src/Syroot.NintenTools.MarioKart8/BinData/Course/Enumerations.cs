﻿#pragma warning disable 1591 // Single elements do not require to be documented.

namespace Syroot.NintenTools.MarioKart8.BinData.Course
{
    /// <summary>
    /// Represents the available section identifiers in a Course.bin file.
    /// </summary>
    public enum SectionIdentifier : uint
    {
        CSPM = 0x4353504D,
        CSTP = 0x43535450
    }
}

#pragma warning restore 1591
