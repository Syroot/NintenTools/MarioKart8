﻿#pragma warning disable 1591 // Single elements do not require to be documented.

namespace Syroot.NintenTools.MarioKart8.BinData.AIRace
{
    /// <summary>
    /// Represents the available section identifiers in an AIRace.bin file.
    /// </summary>
    public enum SectionIdentifier : uint
    {
        ARAP = 0x41524150,
        ARCS = 0x41524353,
        ARLV = 0x41524C56,
        ARIP = 0x41524950,
        ARCF = 0x41524346,
        ARPM = 0x4152504D,
        ARIV = 0x41524956
    }
}

#pragma warning restore 1591
