﻿#pragma warning disable 1591 // Single elements do not require to be documented.

namespace Syroot.NintenTools.MarioKart8.BinData.Parts
{
    /// <summary>
    /// Represents the available section identifiers in a Parts.bin file.
    /// </summary>
    public enum SectionIdentifier : uint
    {
        DTOM = 0x44544F4D,
        DRVR = 0x44525652,
        BODY = 0x424F4459,
        TIRE = 0x54495245,
        WING = 0x57494E47,
        BDDR = 0x42444452,
        WDDR = 0x57444452,
        BDTR = 0x42445452,
        BDWG = 0x42445747,
        TRBD = 0x54524244,
        TIRP = 0x54495250,
        DREF = 0x44524546,
        TIEF = 0x54494546,
        BDEF = 0x42444546
    }
}

#pragma warning restore 1591
