﻿#pragma warning disable 1591 // Single elements do not require to be documented.

namespace Syroot.NintenTools.MarioKart8.BinData
{
    /// <summary>
    /// Represents the file identifiers of <see cref="BinFile"/> instances.
    /// </summary>
    public enum FileIdentifier : uint
    {
        Audio = 0x4155444F, // AUDO
        Item = 0x4954454D, // ITEM
        MiiVoice = 0x4D495643, // MIVC
        Performance = 0x50455246 // PERF
    }
}

#pragma warning restore 1591
