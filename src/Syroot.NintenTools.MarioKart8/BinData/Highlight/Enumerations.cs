﻿#pragma warning disable 1591 // Single elements do not require to be documented.

namespace Syroot.NintenTools.MarioKart8.BinData.Highlight
{
    /// <summary>
    /// Represents the available section identifiers in a Highlight.bin file.
    /// </summary>
    public enum SectionIdentifier : uint
    {
        ID_17A9 = 0x31374139,
        ID_270E = 0x32373045
    }
}

#pragma warning restore 1591
