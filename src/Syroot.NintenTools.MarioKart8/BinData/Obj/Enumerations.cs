﻿#pragma warning disable 1591 // Single elements do not require to be documented.

namespace Syroot.NintenTools.MarioKart8.BinData.Obj
{
    /// <summary>
    /// Represents the available section identifiers in an Obj.bin file.
    /// </summary>
    public enum SectionIdentifier : uint
    {
        OSSP = 0x4F535350,
        OCMY = 0x4F434D59,
        SLPM = 0x534C504D,
        CRCR = 0x43524352,
        JPPK = 0x4A50504B,
        VOLP = 0x564F4C50,
        CAUS = 0x43415553
    }
}

#pragma warning restore 1591
