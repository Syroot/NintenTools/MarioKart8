﻿#pragma warning disable 1591 // Single elements do not require to be documented.

namespace Syroot.NintenTools.MarioKart8.BinData.Item
{
    /// <summary>
    /// Represents the available section identifiers in an Item.bin file.
    /// </summary>
    public enum SectionIdentifier : uint
    {
        RaceTitleSets = 0x49545354, // ITST
        RaceSets = 0x49545332, // ITS2
        RaceDistances = 0x49544453, // ITDS
        BattleTitleSets = 0x49545333, // ITS3
        BattleSets = 0x49545334, // ITS4
        BattleDistances = 0x49545254, // ITRT
        ITDR = 0x49544452,
        ITRG = 0x49545247,
        ITGF = 0x49544746,
        ITDP = 0x49544450
    }

    /// <summary>
    /// Represents the item sets available for versus races.
    /// </summary>
    public enum RaceItemSet
    {
        GrandPrix, GrandPrixAI,
        All, AllAI,
        Mushrooms, MushroomsAI,
        Shells, ShellsAI,
        Bananas, BananasAI,
        Bobombs, BobombsAI,
        UnusedMushrooms1, UnusedMushrooms1AI,
        UnusedShells,
        UnusedBananas,
        UnusedBobombs,
        UnusedMushrooms2,
        Frantic, FranticAI
    }

    /// <summary>
    /// Represents the distances used according to AI players participating in the race.
    /// </summary>
    public enum RaceDistanceType
    {
        NoAI,
        WithAI
        // 18 groups are unused
    }

    /// <summary>
    /// Represents the item sets available for battle mode.
    /// </summary>
    public enum BattleItemSet
    {
        All, AllAI,
        Mushrooms, MushroomsAI,
        Shells, ShellsAI,
        Bananas, BananasAI,
        Bobombs, BobombsAI,
        UnusedShellsBananas1,
        UnusedMushrooms,
        UnusedShells,
        UnusedBananas,
        UnusedBobombs,
        UnusedShellsBananas2,
        Frantic, FranticAI
    }

    /// <summary>
    /// Represents the distance game states in battle modes.
    /// </summary>
    public enum BattleDistanceType
    {
        Minute2_2PlusBalloons,
        Minute2_2Balloons,
        Minute2_1Balloon,
        Minute1_2PlusBalloons,
        Minute1_2Balloons,
        Minute1_1Balloon,
        Minute0_2PlusBalloons,
        Minute0_2Balloons,
        Minute0_1Balloon,
        NoBalloons
    }
}

#pragma warning restore 1591
