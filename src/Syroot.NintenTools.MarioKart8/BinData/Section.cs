﻿using System;
using System.Text;

namespace Syroot.NintenTools.MarioKart8.BinData
{
    /// <summary>
    /// Represents a section in a <see cref="BinFile"/>.
    /// </summary>
    public class Section
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the unique identifier of this section.
        /// </summary>
        public uint ID { get; set; }

        /// <summary>
        /// Gets or sets a parameter typically specifying a number of elements in an instance.
        /// </summary>
        public ushort ParamCount { get; set; }

        /// <summary>
        /// Gets or sets a parameter typically specifying how many instances are available.
        /// </summary>
        public ushort ParamRepeat { get; set; }

        /// <summary>
        /// Gets or sets a parameter only available in Mario Kart 8 Deluxe files.
        /// </summary>
        public uint ParamDeluxe { get; set; }

        /// <summary>
        /// Gets or sets an unknown parameter typically specifying the instance data format.
        /// </summary>
        public uint ParamUnknown { get; set; }

        /// <summary>
        /// Gets or sets the raw bytes of this section.
        /// </summary>
        public ISectionData Data { get; set; }
        
        /// <summary>
        /// Gets a readable representation of the <see cref="ID"/>.
        /// </summary>
        public string Name
        {
            get
            {
                byte[] bytes = BitConverter.GetBytes(ID);
                Array.Reverse(bytes);
                return Encoding.ASCII.GetString(bytes);
            }
        }
    }
}
