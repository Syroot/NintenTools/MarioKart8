﻿#pragma warning disable 1591 // Single elements do not require to be documented.

namespace Syroot.NintenTools.MarioKart8.BinData.UI
{
    /// <summary>
    /// Represents the available section identifiers in a UI.bin file.
    /// </summary>
    public enum SectionIdentifier
    {
        UIMM = 0x55494D4D
    }
}

#pragma warning restore 1591
