﻿#pragma warning disable 1591 // Single elements do not require to be documented.

namespace Syroot.NintenTools.MarioKart8.BinData.OpenFlag
{
    /// <summary>
    /// Represents the available section identifiers in an OpenFlag.bin file.
    /// </summary>
    public enum SectionIdentifier : uint
    {
        OFPT = 0x4F465054,
        OFDF = 0x4F464446,
        OFBF = 0x4F464246,
        OFTF = 0x4F465446,
        OFWF = 0x4F465746
    }
}

#pragma warning restore 1591
