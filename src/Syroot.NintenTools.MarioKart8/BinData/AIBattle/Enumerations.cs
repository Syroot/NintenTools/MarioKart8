﻿#pragma warning disable 1591 // Single elements do not require to be documented.

namespace Syroot.NintenTools.MarioKart8.BinData.AIBattle
{
    /// <summary>
    /// Represents the available section identifiers in an AIBattle.bin file.
    /// </summary>
    public enum SectionIdentifier : uint
    {
        ABLB = 0x41424C42,
        ABKD = 0x41424B44,
        ABBB = 0x41424242,
        ABCN = 0x4142434E,
        ABSN = 0x4142534E,
        ABPA = 0x41425041
    }
}

#pragma warning restore 1591
