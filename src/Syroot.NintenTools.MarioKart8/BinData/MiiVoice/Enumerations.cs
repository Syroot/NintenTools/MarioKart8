﻿#pragma warning disable 1591 // Single elements do not require to be documented.

namespace Syroot.NintenTools.MarioKart8.BinData.MiiVoice
{
    /// <summary>
    /// Represents the available section identifiers in a MiiVoice.bin file.
    /// </summary>
    public enum SectionIdentifier : uint
    {
        MIVI = 0x4D495649,
        MIVF = 0x4D495646,
        MNFP = 0x4D4E4650
    }
}

#pragma warning restore 1591
