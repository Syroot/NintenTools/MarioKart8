using System.IO;

namespace Syroot.NintenTools.MarioKart8
{
    /// <summary>
    /// Represents data which can be saved by providing a <see cref="Stream "/>to write to.
    /// </summary>
    public interface ISaveable
    {
        // ---- METHODS ------------------------------------------------------------------------------------------------

        /// <summary>
        /// Saves the data into the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to save the data to.</param>
        /// <param name="leaveOpen"><c>true</c> to leave <paramref name="stream"/> open after saving the instance.
        /// </param>
        void Save(Stream stream, bool leaveOpen = false);
    }
}
